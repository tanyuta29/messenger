import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class ServerTest {

    private Server server;
    private static final int PORT = 12345;

    @Before
    public void setup(){
        server =new Server(PORT);
        new Thread(()-> server.start()).start();
    }
@After
    public void tearDown(){
       try {
           server.serverSocket.close();
} catch (IOException e){
           e.printStackTrace();
       }
    }

    @Test
    public void testServerStart(){
        assertTrue(server.serverSocket.isBound());
    }
    @Test
    public void testClientConnection() throws IOException{
        Socket clientSocket = new Socket("localhost", PORT);
   assertTrue(clientSocket.isConnected());
   clientSocket.close();
    }
    @Test
    public void testMessageExchange() throws IOException, InterruptedException{
        Socket client1 = new Socket("localhost", PORT);
        Socket client2 = new Socket("localhost", PORT);
        assertTrue(client1.isConnected());
        assertTrue(client2.isConnected());
        OutputStream output1 = client1.getOutputStream();
        String message1 = "Hello";
        output1.write(message1.getBytes());
        TimeUnit.MILLISECONDS.sleep(100);
        byte[] buffer = new byte[1024];
        int byteRead = client2.getInputStream().read(buffer);
        String receivedMessage = new String(buffer,0,byteRead);
        assertEquals(message1, receivedMessage);

        OutputStream output2 = client2.getOutputStream();
        String message2 = "Hey";
        output2.write(message2.getBytes());
        TimeUnit.MILLISECONDS.sleep(100);
        byteRead = client1.getInputStream().read(buffer);
        receivedMessage = new String(buffer,0,byteRead);
        assertEquals(message2,receivedMessage);
        client1.close();
        client2.close();
    }
    @Test
    public void testConstructor(){
        int port = 12345;
        Server server1 = new Server(port);
        assertTrue(server1.serverSocket !=null);
        assertEquals(port, server1.serverSocket.getLocalPort());
        assertNotNull(server.serverSocket);
        assertEquals(port,server.serverSocket.getLocalPort());
    }
}
