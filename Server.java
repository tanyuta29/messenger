import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {
    protected ServerSocket serverSocket;
    private List<Clients> clients;
    public Server(int port){
        try {
            serverSocket = new ServerSocket(port);
            clients = new ArrayList<>();
            System.out.println("Server on port" + port);
        } catch (IOException e) {
            System.err.println("Error creating socket" + e.getMessage());
        }
    }
public void start(){
        while (true){
            try{
                Socket clientSocket = serverSocket.accept();
                System.out.println("New client connected");
                Clients client = new Clients(clientSocket);
                clients.add(client);
                new Thread(client).start();
            }catch (IOException e){
                System.err.println( "Connect mistake with client" + e.getMessage());
            }
        }
}

    public static void main(String[] args) {
        int port = 12345;
        Server server = new Server(port);
        server.start();
    }
    private class Clients implements Runnable {
        private Socket clientSocket;
        private InputStream input;
        private OutputStream output;

        public Clients(Socket clientSocket){
            try {
                this.clientSocket=clientSocket;
                input = clientSocket.getInputStream();
                output = clientSocket.getOutputStream();
            } catch (IOException e){
                System.err.println("Error creating client handler: " + e.getMessage());
            }
        }

        @Override
        public void run() {
            try {
                byte[] buffer = new byte [1024];
                int bytesRead;
                while ((bytesRead = input.read(buffer)) != -1){
                    String message = new String(buffer, 0, bytesRead);
                    System.out.println(" Received from client: " + message);
                    for (Clients clients1 : clients){
                        if(clients1 != this){
                            clients1.send(message);
                        }
                    }
                }
            }catch (IOException e){
                System.err.println("Error handling client " + e.getMessage());
            } finally {
                clients.remove(this);
                try {
                    clientSocket.close();
                }catch (IOException e){
                    System.err.println(" Error closing client socket: " + e.getMessage());
                }
            }
        }
        private void send(String message) throws IOException{
            output.write(message.getBytes());
        }
    }}
