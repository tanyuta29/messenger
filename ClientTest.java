import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.assertEquals;

public class ClientTest {
    private static final int PORT = 12345;
    private Server server;
    private Client client;
    @Before
    public void setup(){
        server = new Server(PORT);
        new Thread(()-> server.start()).start();
        client = new Client("localhost", PORT);
    }
    @After
    public void tearDown(){
        client.disconnect();
    }
    @Test
    public void testSendMessageAndReceiveMessage(){
        String message = "Hey!";
        client.sendMessage(message);
        String receivedMessage = client.receiveMessage();
        Assert.assertNotNull(receivedMessage);
        Assert.assertEquals(message, receivedMessage);
    }
@Test
  public void TestDisconnect(){
        Assert.assertTrue(client.isConnected());
        client.disconnect();
}
@Test
public void testMultiClientMessageExchange() throws InterruptedException {

    Client client1 = new Client("localhost", PORT);
    Client client2 = new Client("localhost", PORT);

    String message1 = "Hello, Client 1!";
    String message2 = "Hi, Client 2!";

    client1.sendMessage(message1);
    client2.sendMessage(message2);
    TimeUnit.MILLISECONDS.sleep(100);

    String receivedMessage1 = client1.receiveMessage();
    String receivedMessage2 = client2.receiveMessage();

    assertEquals(message1, receivedMessage1);
    assertEquals(message2, receivedMessage2);

    client1.disconnect();
    client2.disconnect();
}
}

