import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

public class Client {
        private Socket socket;
        private BufferedReader input;
        private OutputStream output;

        public Client(String host, int port) {
                try {
                        socket = new Socket(host, port);
                        input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                        output = socket.getOutputStream();
                } catch (IOException e) {
                        System.err.println("Error creating client: " + e.getMessage());
                }
        }

        public void sendMessage(String message) {
                try {
                        output.write(message.getBytes());
                        output.flush();
                } catch (IOException e) {
                        System.err.println("Error sending message: " + e.getMessage());
                }
        }

        public String receiveMessage() {
                try {
                        return input.readLine();
                } catch (IOException e) {
                        System.err.println("Error receiving message: " + e.getMessage());
                }
                return null;
        }

        public void disconnect() {
                try {
                        socket.close();
                } catch (IOException e) {
                        System.err.println("Error disconnecting client: " + e.getMessage());
                }
        }

        public boolean isConnected() {
                return socket.isConnected();
        }
}
